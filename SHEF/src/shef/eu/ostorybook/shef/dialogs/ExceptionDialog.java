package eu.ostorybook.shef.dialogs;

import java.awt.Dialog;
import java.awt.Dimension;
import java.awt.Frame;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Insets;
import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSeparator;
import javax.swing.JTextArea;
import javax.swing.UIManager;
import eu.ostorybook.shef.I18Nmsg;

public class ExceptionDialog extends JDialog {

	private static final I18Nmsg i18n = I18Nmsg.getInstance();
	private static final long serialVersionUID = 1L;
	private static final int PREFERRED_WIDTH = 450;
	private static final String DEFAULT_TITLE = i18n.str("error");
	private JPanel jContentPane = null;
	private JLabel iconLabel = null;
	private JLabel titleLabel = null;
	private JLabel msgLabel = null;
	private JPanel buttonPanel = null;
	private JButton okButton = null;
	private JButton detailsButton = null;
	private JScrollPane scrollPane = null;
	private JTextArea textArea = null;
	private JSeparator separator = null;

	public ExceptionDialog() {
		super();
		init(new Exception());

	}

	/**
	 * @param owner
	 */
	public ExceptionDialog(Frame owner, Throwable th) {
		super(owner, DEFAULT_TITLE);
		init(th);

	}

	public ExceptionDialog(Dialog owner, Throwable th) {
		super(owner, DEFAULT_TITLE);
		init(th);
	}

	private void init(Throwable th) {
		setModal(true);
		initialize();
		setThrowable(th);
		showDetails(false);
	}

	/**
	 * This method initializes this
	 *
	 * @return void
	 */
	private void initialize() {
		this.setContentPane(getJContentPane());
		getRootPane().setDefaultButton(getOkButton());
	}

	/**
	 * This method initializes jContentPane
	 *
	 * @return javax.swing.JPanel
	 */
	private JPanel getJContentPane() {
		if (jContentPane == null) {
			jContentPane = new JPanel();
			jContentPane.setLayout(new GridBagLayout());
			jContentPane.setBorder(BorderFactory.createEmptyBorder(12, 5, 10, 5));

			iconLabel = new JLabel();
			iconLabel.setText("");
			iconLabel.setIcon(UIManager.getIcon("OptionPane.errorIcon"));
			GBC gbc00 = new GBC(0,0);
			gbc00.gridheight = 3;
			gbc00.anchor = GridBagConstraints.NORTH;
			gbc00.weighty = 0.0;
			gbc00.insets = new Insets(0, 0, 0, 0);
			jContentPane.add(iconLabel, gbc00);
			titleLabel = new JLabel();
			titleLabel.setText(i18n.str("error_prompt"));
			GBC gbc01 = new GBC(0, 1, GridBagConstraints.NONE);
			gbc01.anchor = GridBagConstraints.WEST;
			gbc01.insets = new Insets(0, 10, 5, 5);
			gbc01.weightx = 1.0;
			jContentPane.add(titleLabel, gbc01);
			msgLabel = new JLabel();
			msgLabel.setText("");
			GBC gbc11 = new GBC(1, 1);
			gbc11.anchor = GridBagConstraints.WEST;
			gbc11.insets = new Insets(0, 20, 5, 5);
			jContentPane.add(msgLabel, gbc11);
			GBC gbc02 = new GBC(0, 2, GridBagConstraints.VERTICAL);
			gbc02.gridheight = 3;
			gbc02.insets = new Insets(0, 0, 10, 0);
			gbc02.anchor = GridBagConstraints.WEST;
			jContentPane.add(getButtonPanel(), gbc02);
			GBC gbc40 = new GBC(4, 0, GridBagConstraints.BOTH);
			gbc40.weightx = 1.0;
			gbc40.weighty = 1.0;
			gbc40.gridwidth = 3;
			gbc40.insets = new Insets(0, 0, 0, 0);
			jContentPane.add(getScrollPane(), gbc40);
			GBC gbc30 = new GBC(3, 0, GridBagConstraints.HORIZONTAL);
			gbc30.gridwidth = 3;
			gbc30.anchor = GridBagConstraints.WEST;
			gbc30.insets = new Insets(0, 0, 0, 0);
			jContentPane.add(getSeparator(), gbc30);
		}
		return jContentPane;
	}

	/**
	 * This method initializes buttonPanel
	 *
	 * @return javax.swing.JPanel
	 */
	private JPanel getButtonPanel() {
		if (buttonPanel == null) {
			GridLayout gridLayout = new GridLayout();
			gridLayout.setRows(2);
			gridLayout.setHgap(0);
			gridLayout.setVgap(5);
			gridLayout.setColumns(1);
			buttonPanel = new JPanel();
			buttonPanel.setLayout(gridLayout);
			buttonPanel.add(getOkButton(), null);
			buttonPanel.add(getDetailsButton(), null);
		}
		return buttonPanel;
	}

	/**
	 * This method initializes okButton
	 *
	 * @return javax.swing.JButton
	 */
	private JButton getOkButton() {
		if (okButton == null) {
			okButton = new JButton();
			okButton.setText(i18n.str("ok"));
			okButton.addActionListener((java.awt.event.ActionEvent e) -> {
				dispose();
			});
		}
		return okButton;
	}

	/**
	 * This method initializes detailsButton
	 *
	 * @return javax.swing.JButton
	 */
	private JButton getDetailsButton() {
		if (detailsButton == null) {
			detailsButton = new JButton();
			detailsButton.setText(i18n.str("_details"));
			detailsButton.addActionListener((java.awt.event.ActionEvent e) -> {
				toggleDetails();
			});
		}
		return detailsButton;
	}

	private void toggleDetails() {
		showDetails(!isDetailsVisible());
	}

	public void showDetails(boolean b) {
		if (b) {
			detailsButton.setText(i18n.str("details_"));
			scrollPane.setVisible(true);
			separator.setVisible(false);
		} else {
			detailsButton.setText(i18n.str("_details"));
			scrollPane.setVisible(false);
			separator.setVisible(true);
		}
		setResizable(true);
		pack();
		setResizable(false);
	}

	public boolean isDetailsVisible() {
		return scrollPane.isVisible();
	}

	public void setThrowable(Throwable th) {
		String msg = i18n.str("no_message_given");
		if (th.getLocalizedMessage() != null && !th.getLocalizedMessage().equals("")) {
			msg = th.getLocalizedMessage();
		}
		msgLabel.setText(msg);
		ByteArrayOutputStream bout = new ByteArrayOutputStream();
		th.printStackTrace(new PrintStream(bout));
		String stackTrace = new String(bout.toByteArray());
		textArea.setText(stackTrace);
		textArea.setCaretPosition(0);
	}

	/**
	 * @return a JSeparator that gets swapped out with the detail pane.
	 */
	private JSeparator getSeparator() {
		if (separator == null) {
			separator = new JSeparator();
			separator.setPreferredSize(new Dimension(PREFERRED_WIDTH, 3));
		}
		return separator;
	}

	/**
	 * This method initializes scrollPane
	 *
	 * @return javax.swing.JScrollPane
	 */
	private JScrollPane getScrollPane() {
		if (scrollPane == null) {
			scrollPane = new JScrollPane();
			scrollPane.setPreferredSize(new Dimension(PREFERRED_WIDTH, 200));
			scrollPane.setViewportView(getTextArea());
		}
		return scrollPane;
	}

	/**
	 * This method initializes textArea
	 *
	 * @return javax.swing.JTextArea
	 */
	private JTextArea getTextArea() {
		if (textArea == null) {
			textArea = new JTextArea();
			textArea.setTabSize(2);
			textArea.setEditable(false);
			textArea.setOpaque(false);
		}
		return textArea;
	}
}
