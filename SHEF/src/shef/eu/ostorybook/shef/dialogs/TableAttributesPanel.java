/*
 * Created on Dec 22, 2005
 *
 */
package eu.ostorybook.shef.dialogs;

import java.awt.GridBagLayout;
import java.util.Hashtable;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JPanel;
import javax.swing.JSpinner;
import javax.swing.SpinnerNumberModel;

public class TableAttributesPanel extends HTMLAttributeEditorPanel {

	private static final String ALIGNMENTS[] = {"left", "center", "right"}; //$NON-NLS-2$ //$NON-NLS-3$
	private static final String MEASUREMENTS[] = {"percent", "pixels"}; //$NON-NLS-2$
	private JCheckBox widthCB = null;
	private JSpinner widthField = null;
	private JComboBox widthCombo = null;
	private JCheckBox alignCB = null;
	private JCheckBox cellSpacingCB = null;
	private JSpinner cellSpacingField = null;
	private JCheckBox borderCB = null;
	private JSpinner borderField = null;
	private JCheckBox cellPaddingCB = null;
	private JSpinner cellPaddingField = null;
	private JComboBox alignCombo = null;
	private BGColorPanel bgPanel = null;
	private JPanel expansionPanel = null;

	/**
	 * This is the default constructor
	 */
	public TableAttributesPanel() {
		this(new Hashtable());
	}

	public TableAttributesPanel(Hashtable attribs) {
		super(attribs);
		initialize();
		updateComponentsFromAttribs();
	}

	@Override
	public void updateComponentsFromAttribs() {
		if (attribs.containsKey("width")) {
			widthCB.setSelected(true);
			String w = attribs.get("width").toString();
			if (w.endsWith("%")) {
				w = w.substring(0, w.length() - 1);
			} else {
				widthCombo.setSelectedIndex(1);
			}
			widthField.setEnabled(true);

			try {
				widthField.getModel().setValue(w);
			} catch (Exception ex) {
				ex.printStackTrace(System.err);
			}
		} else {
			widthCB.setSelected(false);
			widthField.setEnabled(false);
			widthCombo.setEnabled(false);
		}
		if (attribs.containsKey("align")) {
			alignCB.setSelected(true);
			alignCombo.setEnabled(true);
			alignCombo.setSelectedItem(attribs.get("align"));
		} else {
			alignCB.setSelected(false);
			alignCombo.setEnabled(false);
		}
		if (attribs.containsKey("border")) {
			borderCB.setSelected(true);
			borderField.setEnabled(true);
			try {
				borderField.getModel().setValue(attribs.get("border"));
			} catch (Exception ex) {
				ex.printStackTrace(System.err);
			}
		} else {
			borderCB.setSelected(false);
			borderField.setEnabled(false);
		}
		if (attribs.containsKey("cellpadding")) {
			cellPaddingCB.setSelected(true);
			cellPaddingField.setEnabled(true);
			try {
				cellPaddingField.getModel().setValue(attribs.get("cellpadding"));
			} catch (Exception ex) {
				ex.printStackTrace(System.err);
			}
		} else {
			cellPaddingCB.setSelected(false);
			cellPaddingField.setEnabled(false);
		}
		if (attribs.containsKey("cellspacing")) {
			cellSpacingCB.setSelected(true);
			cellSpacingField.setEnabled(true);
			try {
				cellSpacingField.getModel().setValue(attribs.get("cellspacing").toString());
			} catch (Exception ex) {
				ex.printStackTrace(System.err);
			}
		} else {
			cellSpacingCB.setSelected(false);
			cellSpacingField.setEnabled(false);
		}
		if (attribs.containsKey("bgcolor")) {
			bgPanel.setSelected(true);
			bgPanel.setColor(attribs.get("bgcolor").toString());
		} else {
			bgPanel.setSelected(false);
		}
	}

	@Override
	@SuppressWarnings("unchecked")
	public void updateAttribsFromComponents() {
		if (widthCB.isSelected()) {
			String w = widthField.getModel().getValue().toString();
			if (widthCombo.getSelectedIndex() == 0) {
				w += "%";
			}
			attribs.put("width", w);
		} else {
			attribs.remove("width");
		}

		if (alignCB.isSelected()) {
			attribs.put("align", alignCombo.getSelectedItem().toString());
		} else {
			attribs.remove("align");
		}

		if (borderCB.isSelected()) {
			attribs.put("border",
					borderField.getModel().getValue().toString());
		} else {
			attribs.remove("border");
		}

		if (cellSpacingCB.isSelected()) {
			attribs.put("cellspacing",
					cellSpacingField.getModel().getValue().toString());
		} else {
			attribs.remove("cellspacing");
		}

		if (cellPaddingCB.isSelected()) {
			attribs.put("cellpadding",
					cellPaddingField.getModel().getValue().toString());
		} else {
			attribs.remove("cellpadding");
		}

		if (bgPanel.isSelected()) {
			attribs.put("bgcolor", bgPanel.getColor());
		} else {
			attribs.remove("bgcolor");
		}
	}

	public void setComponentStates(Hashtable attribs) {
		if (attribs.containsKey("width")) {
			widthCB.setSelected(true);
			String w = attribs.get("width").toString();
			if (w.endsWith("%")) {
				w = w.substring(0, w.length() - 1);
			} else {
				widthCombo.setSelectedIndex(1);
			}
			try {
				widthField.getModel().setValue(w);
			} catch (Exception ex) {
				ex.printStackTrace(System.err);
			}
		} else {
			widthCB.setSelected(false);
			widthField.setEnabled(false);
			widthCombo.setEnabled(false);
		}
		if (attribs.containsKey("align")) {
			alignCB.setSelected(true);
			alignCombo.setSelectedItem(attribs.get("align"));
		} else {
			alignCB.setSelected(false);
			alignCombo.setEnabled(false);
		}
		if (attribs.containsKey("border")) {
			borderCB.setSelected(true);
			try {
				borderField.getModel().setValue(attribs.get("border").toString());
			} catch (Exception ex) {
				ex.printStackTrace(System.err);
			}
		} else {
			borderCB.setSelected(false);
			borderField.setEnabled(false);
		}
		if (attribs.containsKey("cellpadding")) {
			cellPaddingCB.setSelected(true);
			try {
				cellPaddingField.getModel().setValue(attribs.get("cellpadding"));
			} catch (Exception ex) {
				ex.printStackTrace(System.err);
			}
		} else {
			cellPaddingCB.setSelected(false);
			cellPaddingField.setEnabled(false);
		}
		if (attribs.containsKey("cellspacing")) {
			cellSpacingCB.setSelected(true);
			try {
				cellSpacingField.getModel().setValue(attribs.get("cellspacing"));
			} catch (Exception ex) {
				ex.printStackTrace(System.err);
			}
		} else {
			cellSpacingCB.setSelected(false);
			cellSpacingField.setEnabled(false);
		}
		if (attribs.containsKey("bgcolor")) {
			bgPanel.setSelected(true);
			bgPanel.setColor(attribs.get("bgcolor").toString());
		} else {
			bgPanel.setSelected(false);
		}
	}

	/**
	 * This method initializes this
	 *
	 * @return void
	 */
	private void initialize() {
		setLayout(new GridBagLayout());
		setSize(320, 140);
		setPreferredSize(new java.awt.Dimension(320, 140));

		add(getWidthCB(), new GBC("0,0,anchor W, ins 0 0 10 3"));
		add(getWidthField(), new GBC("0,1,fill N, wx 0.0,anchor W, ins 0 0 10 0"));
		add(getWidthCombo(), new GBC("0,2,fill N, wx 0.0, anchor W, width 2, ins 0 0 10 0"));
		add(getAlignCB(), new GBC("1,0,anchor W, ins 0 0 2 3"));
		add(getCellSpacingCB(), new GBC("1,3,anchor W, ins 0 0 2 3"));
		add(getCellSpacingField(), new GBC("1,4,fill N, wx 1.0, anchor W, ins 0 0 2 0"));
		add(getBorderCB(), new GBC("2,0,anchor W, ins 0 0 10 3"));
		add(getBorderField(), new GBC("2,1, fill N, wx 0.0, anchor W, ins 0 0 10 15"));
		add(getCellPaddingCB(), new GBC("2,3,anchor W,ins 0 0 10 3"));
		add(getCellPaddingField(), new GBC("2,4,fill N, wx 1.0,anchor W, ins 0 0 10 0"));
		add(getAlignCombo(), new GBC("1,1,fill N, wx 0.0, width 2,anchor W, ins 0 0 5 15"));
		add(getBGPanel(), new GBC("3,0, anchor W, width 4, wy 0.0"));
		add(getExpansionPanel(), new GBC("4,0,fill H, anchor W,width 4, wx 0.0, wy 1.0"));

	}

	/**
	 * This method initializes widthCB
	 *
	 * @return javax.swing.JCheckBox
	 */
	private JCheckBox getWidthCB() {
		if (widthCB == null) {
			widthCB = new JCheckBox();
			widthCB.setText(i18n.str("width"));
			widthCB.addItemListener((java.awt.event.ItemEvent e) -> {
				widthField.setEnabled(widthCB.isSelected());
				widthCombo.setEnabled(widthCB.isSelected());
			});
		}
		return widthCB;
	}

	/**
	 * This method initializes widthField
	 *
	 * @return javax.swing.JSpinner
	 */
	private JSpinner getWidthField() {
		if (widthField == null) {
			widthField = new JSpinner(new SpinnerNumberModel(100, 1, 999, 1));

		}
		return widthField;
	}

	/**
	 * This method initializes widthCombo
	 *
	 * @return javax.swing.JComboBox
	 */
	@SuppressWarnings("unchecked")
	private JComboBox getWidthCombo() {
		if (widthCombo == null) {
			widthCombo = new JComboBox(MEASUREMENTS);
		}
		return widthCombo;
	}

	/**
	 * This method initializes alignCB
	 *
	 * @return javax.swing.JCheckBox
	 */
	private JCheckBox getAlignCB() {
		if (alignCB == null) {
			alignCB = new JCheckBox();
			alignCB.setText(i18n.str("align"));
			alignCB.addItemListener((java.awt.event.ItemEvent e) -> {
				alignCombo.setEnabled(alignCB.isSelected());
			});
		}
		return alignCB;
	}

	/**
	 * This method initializes cellSpacingCB
	 *
	 * @return javax.swing.JCheckBox
	 */
	private JCheckBox getCellSpacingCB() {
		if (cellSpacingCB == null) {
			cellSpacingCB = new JCheckBox();
			cellSpacingCB.setText(i18n.str("cellspacing"));
			cellSpacingCB.addItemListener((java.awt.event.ItemEvent e) -> {
				cellSpacingField.setEnabled(cellSpacingCB.isSelected());
			});
		}
		return cellSpacingCB;
	}

	/**
	 * This method initializes cellSpacingField
	 *
	 * @return javax.swing.JSpinner
	 */
	private JSpinner getCellSpacingField() {
		if (cellSpacingField == null) {
			cellSpacingField = new JSpinner(new SpinnerNumberModel(1, 0, 999, 1));

		}
		return cellSpacingField;
	}

	/**
	 * This method initializes borderCB
	 *
	 * @return javax.swing.JCheckBox
	 */
	private JCheckBox getBorderCB() {
		if (borderCB == null) {
			borderCB = new JCheckBox();
			borderCB.setText(i18n.str("border"));
			borderCB.addItemListener((java.awt.event.ItemEvent e) -> {
				borderField.setEnabled(borderCB.isSelected());
			});
		}
		return borderCB;
	}

	/**
	 * This method initializes borderField
	 *
	 * @return javax.swing.JSpinner
	 */
	private JSpinner getBorderField() {
		if (borderField == null) {
			borderField = new JSpinner(new SpinnerNumberModel(1, 0, 999, 1));

		}
		return borderField;
	}

	/**
	 * This method initializes cellPaddingCB
	 *
	 * @return javax.swing.JCheckBox
	 */
	private JCheckBox getCellPaddingCB() {
		if (cellPaddingCB == null) {
			cellPaddingCB = new JCheckBox();
			cellPaddingCB.setText(i18n.str("cellpadding"));
			cellPaddingCB.addItemListener((java.awt.event.ItemEvent e) -> {
				cellPaddingField.setEnabled(cellPaddingCB.isSelected());
			});
		}
		return cellPaddingCB;
	}

	/**
	 * This method initializes cellPaddingField
	 *
	 * @return javax.swing.JSpinner
	 */
	private JSpinner getCellPaddingField() {
		if (cellPaddingField == null) {
			cellPaddingField = new JSpinner(new SpinnerNumberModel(1, 0, 999, 1));

		}
		return cellPaddingField;
	}

	/**
	 * This method initializes alignCombo
	 *
	 * @return javax.swing.JComboBox
	 */
	@SuppressWarnings("unchecked")
	private JComboBox getAlignCombo() {
		if (alignCombo == null) {
			alignCombo = new JComboBox(ALIGNMENTS);
		}
		return alignCombo;
	}

	/**
	 * This method initializes tempPanel
	 *
	 * @return javax.swing.JPanel
	 */
	private JPanel getBGPanel() {
		if (bgPanel == null) {
			bgPanel = new BGColorPanel();

		}
		return bgPanel;
	}

	/**
	 * This method initializes expansionPanel
	 *
	 * @return javax.swing.JPanel
	 */
	private JPanel getExpansionPanel() {
		if (expansionPanel == null) {
			expansionPanel = new JPanel();
		}
		return expansionPanel;
	}

}
