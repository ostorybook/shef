/*
 * Created on Jan 14, 2006
 *
 */
package eu.ostorybook.shef.dialogs;

import eu.ostorybook.shef.resources.images.Images;
import java.awt.Dialog;
import java.awt.Frame;
import java.util.Iterator;
import java.util.Map;
import javax.swing.Icon;

public class ImageDialog extends HTMLOptionDialog {

	private static Icon icon = Images.getIcon("image");
	private static String title = i18n.str("image");
	private static String desc = i18n.str("image_desc");
	private ImagePanel imagePanel;

	public ImageDialog(Frame parent) {
		super(parent, title, desc, icon);
		init();
	}

	public ImageDialog(Dialog parent) {
		super(parent, title, desc, icon);
		init();
	}

	private void init() {
		imagePanel = new ImagePanel();
		setContentPane(imagePanel);
		//setSize(300, 345);
		pack();
		setResizable(false);
	}

	public void setImageAttributes(Map attr) {
		imagePanel.setAttributes(attr);
	}

	public Map getImageAttributes() {
		return imagePanel.getAttributes();
	}

	private String createImgAttributes(Map ht) {
		String html = "";
		for (Iterator e = ht.keySet().iterator(); e.hasNext();) {
			Object k = e.next();
			if (k.toString().equals("a") || k.toString().equals("name")) {
				continue;
			}
			html += " " + k + "=" + "\"" + ht.get(k) + "\"";
		}
		return html;
	}

	@Override
	public String getHTML() {
		Map imgAttr = imagePanel.getAttributes();
		boolean hasLink = imgAttr.containsKey("a");
		String html = "";
		if (hasLink) {
			html = "<a " + imgAttr.get("a") + ">";
		}
		html += "<img" + createImgAttributes(imgAttr) + ">";
		if (hasLink) {
			html += "</a>";
		}
		return html;
	}

}
