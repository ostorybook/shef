/*
 * Created on Jan 10, 2006
 *
 */
package eu.ostorybook.shef.dialogs;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GradientPaint;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.GridBagLayout;
import java.awt.Paint;
import java.awt.Rectangle;
import java.awt.RenderingHints;
import javax.swing.BorderFactory;
import javax.swing.Icon;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;

public class HeaderPanel extends JPanel {

	private JLabel titleLabel = null;
	private JLabel msgLabel = null;
	private JLabel iconLabel = null;

	/**
	 * This is the default constructor
	 */
	public HeaderPanel() {
		super();
		initialize();
	}

	public HeaderPanel(String title, String desc, Icon ico) {
		super();
		initialize();
		setTitle(title);
		setDescription(desc);
		setIcon(ico);
	}

	public void setTitle(String title) {
		titleLabel.setText(title);
	}

	public void setDescription(String desc) {
		msgLabel.setText(desc);
	}

	public void setIcon(Icon icon) {
		iconLabel.setIcon(icon);
	}

	/**
	 * This method initializes this
	 *
	 * @return void
	 */
	private void initialize() {
		setLayout(new GridBagLayout());
		setSize(360, 56);
		setPreferredSize(new Dimension(360, 56));
		setBorder(BorderFactory.createLineBorder(Color.black, 1));

		titleLabel = new JLabel();
		titleLabel.setFont(new Font("Dialog", Font.BOLD, 16));
		titleLabel.setVerticalAlignment(SwingConstants.TOP);
		add(titleLabel, new GBC("0,0,fill H, width 2, wx 0.0, wy 0.0, anchor W, ins 6 10 0 0"));

		msgLabel = new JLabel();
		msgLabel.setFont(new Font("Dialog", Font.PLAIN, 12));
		msgLabel.setVerticalAlignment(SwingConstants.TOP);
		add(msgLabel, new GBC("1,0, fill B, wx 1.0, wy 1.0, anchor W, ins 2 25 0 0"));

		iconLabel = new JLabel();
		iconLabel.setVerticalAlignment(SwingConstants.CENTER);
		add(iconLabel, new GBC("0,1, height 2, ins 0 5 0 10"));
	}

	@Override
	protected void paintComponent(Graphics g) {
		super.paintComponent(g);
		Graphics2D g2 = (Graphics2D) g;
		g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
		Rectangle bounds = getBounds();
		// Set Paint for filling Shape
		Color blue = new Color(153, 204, 255);
		Paint gradientPaint = new GradientPaint(bounds.width * 0.5f, bounds.y, Color.white, bounds.width, 0f, blue);
		g2.setPaint(gradientPaint);
		g2.fillRect(0, 0, bounds.width, bounds.height);
	}

}
