/*
 * Created on Jan 17, 2006
 *
 */
package eu.ostorybook.shef.dialogs;

import eu.ostorybook.shef.actions.TextEditPopupManager;
import java.awt.GridBagLayout;
import java.util.Hashtable;
import javax.swing.JLabel;
import javax.swing.JTextField;

public class StyleAttributesPanel extends HTMLAttributeEditorPanel {

	private JLabel classLabel = null;
	private JLabel idLabel = null;
	private JTextField classField = null;
	private JTextField idField = null;

	/**
	 * This method initializes
	 *
	 */
	public StyleAttributesPanel() {
		this(new Hashtable());
	}

	public StyleAttributesPanel(Hashtable attr) {
		super();
		initialize();
		setAttributes(attr);
		this.updateComponentsFromAttribs();
	}

	/**
	 * This method initializes this
	 *
	 */
	private void initialize() {
		setLayout(new GridBagLayout());
		setSize(new java.awt.Dimension(210, 60));
		setPreferredSize(new java.awt.Dimension(210, 60));
		setBorder(javax.swing.BorderFactory.createEmptyBorder(5, 5, 5, 5));

		classLabel = new JLabel(i18n.str("class"));
		add(classLabel, new GBC("0,0,anchor W, ins 0 0 5 5"));
		add(getClassField(), new GBC("0,1,fill H, wx 1.0, wy 0.0, ins 0 0 5 0"));
		idLabel = new JLabel(i18n.str("id"));
		add(idLabel, new GBC("1,0,anchor W, ins 0 0 5 5"));
		add(getIdField(), new GBC("1,1,fill H, ins 0 0 5 0, wx 1.0"));

		TextEditPopupManager popupMan = TextEditPopupManager.getInstance();
		popupMan.registerJTextComponent(classField);
		popupMan.registerJTextComponent(idField);

	}

	@Override
	public void updateComponentsFromAttribs() {
		if (attribs.containsKey("class")) {
			classField.setText(attribs.get("class").toString());
		} else {
			classField.setText("");
		}
		if (attribs.containsKey("id")) {
			idField.setText(attribs.get("id").toString());
		} else {
			idField.setText("");
		}
	}

	@Override
	@SuppressWarnings("unchecked")
	public void updateAttribsFromComponents() {
		if (!classField.getText().equals("")) {
			attribs.put("class", classField.getText());
		} else {
			attribs.remove("class");
		}
		if (!idField.getText().equals("")) {
			attribs.put("id", idField.getText());
		} else {
			attribs.remove("id");
		}
	}

	/**
	 * This method initializes classField
	 *
	 * @return javax.swing.JTextField
	 */
	private JTextField getClassField() {
		if (classField == null) {
			classField = new JTextField();
		}
		return classField;
	}

	/**
	 * This method initializes idField
	 *
	 * @return javax.swing.JTextField
	 */
	private JTextField getIdField() {
		if (idField == null) {
			idField = new JTextField();
		}
		return idField;
	}

}
