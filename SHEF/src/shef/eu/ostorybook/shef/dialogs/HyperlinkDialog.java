/*
 * Created on Jan 14, 2006
 *
 */
package eu.ostorybook.shef.dialogs;

import eu.ostorybook.shef.resources.images.Images;
import java.awt.Dialog;
import java.awt.Frame;
import java.util.Iterator;
import java.util.Map;
import javax.swing.BorderFactory;
import javax.swing.Icon;

public class HyperlinkDialog extends HTMLOptionDialog {

	private static Icon icon = Images.getIcon("link");
	private static String title = i18n.str("hyperlink");
	private static String desc = i18n.str("hyperlink_desc");
	private LinkPanel linkPanel;

	public HyperlinkDialog(Frame parent) {
		this(parent, title, desc, icon, true);
	}

	public HyperlinkDialog(Dialog parent) {
		this(parent, title, desc, icon, true);
	}

	public HyperlinkDialog(Dialog parent, String title, String desc, Icon ico, boolean urlFieldEnabled) {
		super(parent, title, desc, ico);
		init(urlFieldEnabled);
	}

	public HyperlinkDialog(Frame parent, String title, String desc, Icon ico, boolean urlFieldEnabled) {
		super(parent, title, desc, ico);
		init(urlFieldEnabled);
	}

	private void init(boolean urlFieldEnabled) {
		linkPanel = new LinkPanel(urlFieldEnabled);
		linkPanel.setBorder(BorderFactory.createEmptyBorder(5, 5, 5, 5));
		setContentPane(linkPanel);
		setSize(315, 370);
		setResizable(false);
	}

	public Map getAttributes() {
		return linkPanel.getAttributes();
	}

	public void setAttributes(Map attribs) {
		linkPanel.setAttributes(attribs);
	}

	public void setLinkText(String text) {
		linkPanel.setLinkText(text);
	}

	public String getLinkText() {
		return linkPanel.getLinkText();
	}

	@Override
	public String getHTML() {
		String html = "<a";
		Map ht = getAttributes();
		for (Iterator e = ht.keySet().iterator(); e.hasNext();) {
			Object k = e.next();
			html += " " + k + "=" + "\"" + ht.get(k) + "\"";
		}
		html += ">" + getLinkText() + "</a>";
		return html;
	}

}
