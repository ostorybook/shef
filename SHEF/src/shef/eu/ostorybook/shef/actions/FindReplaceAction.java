/*
 * Created on Jan 24, 2006
 *
 */
package eu.ostorybook.shef.actions;

import eu.ostorybook.shef.dialogs.TextFinderDialog;
import java.awt.Component;
import java.awt.Dialog;
import java.awt.Frame;
import java.awt.event.ActionEvent;
import java.awt.event.InputEvent;
import java.awt.event.KeyEvent;
import javax.swing.JEditorPane;
import javax.swing.KeyStroke;
import javax.swing.SwingUtilities;

public class FindReplaceAction extends BasicEditAction {

	private boolean isReplaceTab;
	private TextFinderDialog dialog;

	public FindReplaceAction(boolean isReplace) {
		super(null);
		if (isReplace) {
			putValue(NAME, i18n.str("replace_"));
			putValue(MNEMONIC_KEY, i18n.mnem("replace_"));
		} else {
			putValue(NAME, i18n.str("find_"));
			putValue(MNEMONIC_KEY, i18n.mnem("find_"));
			putValue(ACCELERATOR_KEY,
					KeyStroke.getKeyStroke(KeyEvent.VK_F, InputEvent.CTRL_DOWN_MASK));
		}

		isReplaceTab = isReplace;
	}

	@Override
	protected void doEdit(ActionEvent e, JEditorPane textComponent) {
		Component c = SwingUtilities.getWindowAncestor(textComponent);
		if (dialog == null) {
			if (c instanceof Frame) {
				if (isReplaceTab) {
					dialog = new TextFinderDialog((Frame) c, textComponent, TextFinderDialog.REPLACE);
				} else {
					dialog = new TextFinderDialog((Frame) c, textComponent, TextFinderDialog.FIND);
				}
			} else if (c instanceof Dialog) {
				if (isReplaceTab) {
					dialog = new TextFinderDialog((Dialog) c, textComponent, TextFinderDialog.REPLACE);
				} else {
					dialog = new TextFinderDialog((Dialog) c, textComponent, TextFinderDialog.FIND);
				}
			} else {
				return;
			}
		}
		if (!dialog.isVisible()) {
			dialog.show((isReplaceTab) ? TextFinderDialog.REPLACE : TextFinderDialog.FIND);
		}
	}

	@Override
	protected void updateContextState(JEditorPane editor) {
		if (dialog != null) {
			dialog.setJTextComponent(editor);
		}
	}

}
