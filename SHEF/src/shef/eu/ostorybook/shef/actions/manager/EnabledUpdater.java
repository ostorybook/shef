package eu.ostorybook.shef.actions.manager;

public interface EnabledUpdater extends ShouldBeEnabledDelegate {

	boolean updateEnabled();
}
