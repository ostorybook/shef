package eu.ostorybook.shef.actions.manager;

import java.awt.event.ItemListener;
import javax.swing.Action;

public interface ItemAction extends Action {

	void setSelected(boolean param);

	boolean isSelected();

	void addItemListener(ItemListener param);

	void removeItemListener(ItemListener param);
}
