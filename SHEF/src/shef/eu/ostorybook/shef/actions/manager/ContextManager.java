package eu.ostorybook.shef.actions.manager;

import java.util.Map;

public interface ContextManager {

	void setContext(Map paramMap);

	Map getContext();

	void clearContext();

	void putContextValue(Object obj1, Object obj2);

	Object getContextValue(Object obj);
}
