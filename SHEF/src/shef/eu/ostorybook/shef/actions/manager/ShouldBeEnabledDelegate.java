package eu.ostorybook.shef.actions.manager;

import java.util.EventListener;
import javax.swing.Action;

public interface ShouldBeEnabledDelegate extends EventListener {
  boolean shouldBeEnabled(Action paramAction);
}
