package eu.ostorybook.shef.actions.manager;

import java.awt.event.ActionListener;

public interface Actionable {

	void addActionListener(ActionListener actionListener);

	void removeActionListener(ActionListener actionListener);
}
