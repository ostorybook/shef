package eu.ostorybook.shef.actions;

import eu.ostorybook.shef.actions.manager.ActionBasic;
import eu.ostorybook.shef.actions.manager.EnabledUpdater;
import javax.swing.Action;
import javax.swing.Icon;
import javax.swing.KeyStroke;

public class ActionDefault extends ActionBasic implements EnabledUpdater {

	public ActionDefault() {
		this(null);
	}

	public ActionDefault(String id) {
		this(id, null);
	}

	public ActionDefault(String id, Icon icon) {
		this(id, null, null, icon);
	}

	public ActionDefault(String id, Integer mnemonic, KeyStroke accelerator, Icon icon) {
		this(id, id, id, mnemonic, accelerator, icon);
	}

	public ActionDefault(String id, String shortDesc, String longDesc, Integer mnemonic, KeyStroke accelerator, Icon icon) {
		this(id, id, id, shortDesc, longDesc, mnemonic, accelerator, icon);
	}

	public ActionDefault(String id, String actionName, String actionCommandName, String shortDesc,
			String longDesc, Integer mnemonic, KeyStroke accelerator, Icon icon) {
		this(id, actionName, actionCommandName, shortDesc, longDesc,
				mnemonic, accelerator, icon, false, true);
	}

	public ActionDefault(String id, String actionName, String actionCommandName,
			String shortDesc, String longDesc, Integer mnemonic, KeyStroke accelerator,
			Icon icon, boolean toolbarShowsText, boolean menuShowsIcon) {
		super(id, actionName, actionCommandName, shortDesc, longDesc, mnemonic,
				accelerator, icon, toolbarShowsText, menuShowsIcon);
	}

	@Override
	public boolean updateEnabled() {
		updateEnabledState();
		return isEnabled();
	}

	@Override
	public boolean shouldBeEnabled(Action action) {
		return shouldBeEnabled();
	}

	@Override
	protected void actionPerformedCatch(Throwable t) {
		t.printStackTrace(System.out);
	}
}
