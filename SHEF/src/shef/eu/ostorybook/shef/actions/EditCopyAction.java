/*
 * Created on Nov 2, 2007
 */
package eu.ostorybook.shef.actions;

import java.awt.event.ActionEvent;
import java.awt.event.InputEvent;
import java.awt.event.KeyEvent;
import javax.swing.Action;
import javax.swing.JEditorPane;
import javax.swing.KeyStroke;

/**
 * @author Bob Tantlinger
 *
 */
public class EditCopyAction extends BasicEditAction {

	public EditCopyAction() {
		super("");
		putValue(Action.NAME, i18n.str("copy"));
		setSmallIcon("edit_copy");
		setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_C, InputEvent.CTRL_DOWN_MASK));
		setMnemonic("copy");
		addShouldBeEnabledDelegate((Action a) -> {
			JEditorPane ed = getCurrentEditor();
			return ed != null && ed.getSelectionStart() != ed.getSelectionEnd();
		});
		setShortDescription(i18n.str("copy_desc"));
	}

	@Override
	protected void doEdit(ActionEvent e, JEditorPane editor) {
		editor.copy();
	}

	@Override
	protected void contextChanged() {
		super.contextChanged();
		this.updateEnabledState();
	}

}
