/*
 * Copyright (C) oStorybook Team
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package eu.ostorybook.shef;

import eu.ostorybook.shef.editors.wys.WysiwygEditor;
import eu.ostorybook.shef.editors.src.SourceEditor;
import java.awt.BorderLayout;
import javax.swing.JComponent;
import javax.swing.JPanel;

/**
 *
 * @author favdb
 */
public class SHEFEditor extends JPanel {
	WysiwygEditor wysEditor;
	SourceEditor srcEditor;
	JComponent current;
	private int dlg_lang;
	private boolean reduced;
	
	public SHEFEditor() {
		initUI();
	}

	public SHEFEditor(int dlg_lang) {
		this.dlg_lang=dlg_lang;
		initUI();
	}

	public SHEFEditor(boolean reduced) {
		this.reduced = reduced;
		initUI();
	}

	public SHEFEditor(int dlg_lang, boolean reduced) {
		this.dlg_lang=dlg_lang;
		this.reduced = reduced;
		initUI();
	}

	private void initUI() {
		setLayout(new BorderLayout());
		wysEditor=new WysiwygEditor(this, dlg_lang, reduced);
		current=wysEditor;
		add(wysEditor);
		srcEditor=new SourceEditor(this);
	}

	public void setText(String texte) {
		if (current instanceof WysiwygEditor) {
			wysEditor.setText(texte);
		} else if (current instanceof SourceEditor) {
			srcEditor.setText(texte);
		}
	}

	public void setCaretPosition(int position) {
		if (current instanceof WysiwygEditor) {
			wysEditor.setCaretPosition(position);
		} else if (current instanceof SourceEditor) {
			srcEditor.setCaretPosition(position);
		}
	}

	public String getText() {
		if (current instanceof WysiwygEditor) {
			return wysEditor.getText();
		} else if (current instanceof SourceEditor) {
			return srcEditor.getText();
		}
		return("");
	}

	public void changeTo(String type) {
		System.out.println("change to "+type);
		String texte=getText();
		switch(type) {
			case "wysiwyg":
				this.remove(srcEditor);
				this.add(wysEditor);
				current=wysEditor;
				break;
			case "source":
				this.remove(wysEditor);
				this.add(srcEditor);
				current=srcEditor;
				break;
		}
		setText(texte);
		this.revalidate();
		this.repaint();
	}
	
}
