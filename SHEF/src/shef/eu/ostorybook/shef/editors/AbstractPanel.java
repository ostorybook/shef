/*
 * Copyright (C) 2020 favdb
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package eu.ostorybook.shef.editors;

import eu.ostorybook.shef.I18Nmsg;
import eu.ostorybook.shef.SHEFEditor;
import eu.ostorybook.shef.actions.manager.ActionBasic;
import eu.ostorybook.shef.actions.manager.ActionList;
import eu.ostorybook.shef.resources.images.Images;
import java.awt.BorderLayout;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JPanel;
import javax.swing.JToolBar;

/**
 *
 * @author favdb
 */
public class AbstractPanel extends JPanel {

	public static final I18Nmsg i18n = I18Nmsg.getInstance();
	public final JComponent parent;
	public JToolBar toolbar;
	public ActionList actionList;
	public String changeTo = "Wysiwyg";

	public AbstractPanel() {
		this.parent=null;
	}
	
	public AbstractPanel(SHEFEditor parent) {
		this.parent = parent;
	}

	public void initAll() {
		init();
		initUi();
	}

	public void init() {
		actionList = new ActionList("editor-actions");
		setLayout(new BorderLayout());
		setOpaque(true);
	}

	public void initUi() {
		add(initToolbar(), BorderLayout.NORTH);
	}

	public JToolBar initToolbar() {
		toolbar = new JToolBar();
		return toolbar;
	}

	/**
	 * create a JButton using an ActionBase
	 *
	 * @param actionList
	 * @param act
	 * @return a JButton
	 */
	public JButton initButton(ActionList actionList, ActionBasic act) {
		JButton bt = new JButton(act);
		bt.setName(act.getActionName());
		bt.setText("");
		if (!act.getShortDescription().isEmpty()) {
			bt.setToolTipText(act.getShortDescription());
		}
		bt.setIcon(act.getSmallIcon());
		if (act.getAccelerator() != null) {
			bt.registerKeyboardAction(act,
					act.getAccelerator(),
					JComponent.WHEN_IN_FOCUSED_WINDOW);
		}
		actionList.add(act);
		return (bt);
	}

}
