/*
 * Created on Feb 26, 2005
 *
 */
package eu.ostorybook.shef.editors.wys;

import eu.ostorybook.shef.actions.ActionDefault;
import java.awt.event.ActionEvent;
import javax.swing.Action;
import javax.swing.JEditorPane;
import javax.swing.text.html.HTMLDocument;
import javax.swing.text.html.HTMLEditorKit;

/**
 * @author Bob Tantlinger
 *
 */
public abstract class HTMLTextEditAction extends ActionDefault {

	public static final String EDITOR = "editor";
	public static final int DISABLED = -1, WYSIWYG = 0, SOURCE=1;
	
	public HTMLTextEditAction(String name) {
		super(name);
		addShouldBeEnabledDelegate((Action a) -> getEditMode() != DISABLED);
		updateEnabledState();
	}

	@Override
	public void execute(ActionEvent e) throws Exception {
		if (getEditMode() == WYSIWYG) {
			wysiwygEditPerformed(e, getCurrentEditor());
		}
	}

	public int getEditMode() {
		JEditorPane ep = getCurrentEditor();
		if (ep == null) {
			return DISABLED;
		}
		if (ep.getDocument() instanceof HTMLDocument && ep.getEditorKit() instanceof HTMLEditorKit) {
			return WYSIWYG;
		}
		return SOURCE;
	}

	protected JEditorPane getCurrentEditor() {
		try {
			JEditorPane ep = (JEditorPane) getContextValue(EDITOR);
			return ep;
		} catch (ClassCastException cce) {
		}

		return null;
	}

	@Override
	protected void actionPerformedCatch(Throwable t) {
		t.printStackTrace(System.out);
	}

	@Override
	protected void contextChanged() {
		if (getEditMode() == WYSIWYG) {
			updateWysiwygContextState(getCurrentEditor());
		}
	}

	protected void updateWysiwygContextState(JEditorPane wysEditor) {
	}

	protected abstract void wysiwygEditPerformed(ActionEvent e, JEditorPane editor);

}
