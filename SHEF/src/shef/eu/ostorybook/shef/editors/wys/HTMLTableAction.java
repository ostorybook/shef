/*
 * Created on Feb 26, 2005
 *
 */
package eu.ostorybook.shef.editors.wys;

import eu.ostorybook.shef.actions.CompoundUndoManager;
import eu.ostorybook.shef.dialogs.NewTableDialog;
import eu.ostorybook.shef.resources.images.Images;
import eu.ostorybook.shef.tools.HTMLUtils;
import java.awt.Dialog;
import java.awt.Frame;
import java.awt.Window;
import java.awt.event.ActionEvent;
import java.io.IOException;
import javax.swing.JEditorPane;
import javax.swing.SwingUtilities;
import javax.swing.text.BadLocationException;
import javax.swing.text.Element;
import javax.swing.text.JTextComponent;
import javax.swing.text.html.HTML;
import javax.swing.text.html.HTMLDocument;

/**
 * Action which shows a dialog to insert an HTML table
 *
 * @author Bob Tantlinger
 *
 */
public class HTMLTableAction extends HTMLTextEditAction {

	public HTMLTableAction() {
		super(i18n.str("table_"));
		setMnemonic(i18n.mnem("table_"));
		setSmallIcon(Images.getIcon("table"));
		setShortDescription(i18n.str("table_desc"));
	}

	@Override
	protected void wysiwygEditPerformed(ActionEvent e, JEditorPane editor) {
		NewTableDialog dlg = createNewTableDialog(editor);
		if (dlg == null) {
			return;
		}
		dlg.setLocationRelativeTo(dlg.getParent());
		dlg.setVisible(true);
		if (dlg.hasUserCancelled()) {
			return;
		}
		HTMLDocument document = (HTMLDocument) editor.getDocument();
		String html = dlg.getHTML();
		Element elem = document.getParagraphElement(editor.getCaretPosition());
		CompoundUndoManager.beginCompoundEdit(document);
		try {
			if (HTMLUtils.isElementEmpty(elem)) {
				document.setOuterHTML(elem, html);
			} else if (elem.getName().equals("p-implied")) {
				document.insertAfterEnd(elem, html);
			} else {
				HTMLUtils.insertHTML(html, HTML.Tag.TABLE, editor);
			}
		} catch (IOException | BadLocationException ex) {
			ex.printStackTrace(System.err);
		}
		CompoundUndoManager.endCompoundEdit(document);
	}

	/**
	 * Creates the dialog
	 *
	 * @param ed
	 * @return the dialog
	 */
	private NewTableDialog createNewTableDialog(JTextComponent ed) {
		Window w = SwingUtilities.getWindowAncestor(ed);
		NewTableDialog d = null;
		if (w != null && w instanceof Frame) {
			d = new NewTableDialog((Frame) w);
		} else if (w != null && w instanceof Dialog) {
			d = new NewTableDialog((Dialog) w);
		}
		return d;
	}

}
