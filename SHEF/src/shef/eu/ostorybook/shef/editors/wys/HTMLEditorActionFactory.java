/*
 * Created on Nov 2, 2007
 */
package eu.ostorybook.shef.editors.wys;

import eu.ostorybook.shef.actions.CompoundUndoManager;
import eu.ostorybook.shef.actions.EditCopyAction;
import eu.ostorybook.shef.actions.EditCutAction;
import eu.ostorybook.shef.actions.SelectAllAction;
import eu.ostorybook.shef.editors.wys.HTMLAlignAction;
import eu.ostorybook.shef.editors.wys.HTMLEditPasteAction;
import eu.ostorybook.shef.actions.manager.ActionList;

/**
 * @author Bob Tantlinger
 *
 */
public class HTMLEditorActionFactory {

	public static ActionList createEditActionList() {
		ActionList list = new ActionList("edit");
		list.add(CompoundUndoManager.UNDO);
		list.add(CompoundUndoManager.REDO);
		list.add(null);
		list.add(new EditCutAction());
		list.add(new EditCopyAction());
		list.add(new HTMLEditPasteAction());
		//list.add(new PasteFormattedAction());
		list.add(null);
		list.add(new SelectAllAction());
		//list.add(new IndentAction(IndentAction.INDENT));
		//list.add(new IndentAction(IndentAction.OUTDENT));
		return list;
	}

	public static ActionList createInlineActionList() {
		ActionList list = new ActionList("style");
		list.add(new HTMLInlineAction(HTMLInlineAction.BOLD));
		list.add(new HTMLInlineAction(HTMLInlineAction.ITALIC));
		list.add(new HTMLInlineAction(HTMLInlineAction.UNDERLINE));
		list.add(null);
		list.add(new HTMLInlineAction(HTMLInlineAction.CITE));
		list.add(new HTMLInlineAction(HTMLInlineAction.CODE));
		list.add(new HTMLInlineAction(HTMLInlineAction.EM));
		list.add(new HTMLInlineAction(HTMLInlineAction.STRONG));
		list.add(new HTMLInlineAction(HTMLInlineAction.SUB));
		list.add(new HTMLInlineAction(HTMLInlineAction.SUP));
		list.add(new HTMLInlineAction(HTMLInlineAction.STRIKE));

		return list;
	}

	public static ActionList createAlignActionList() {
		ActionList list = new ActionList("align");
		String[] t = HTMLAlignAction.ALIGNMENTS;
		for (int i = 0; i < t.length; i++) {
			list.add(new HTMLAlignAction(i));
		}

		return list;
	}

	public static ActionList createFontSizeActionList() {
		ActionList list = new ActionList("font-size");
		int[] t = HTMLFontSizeAction.FONT_SIZES;
		for (int i = 0; i < t.length; i++) {
			list.add(new HTMLFontSizeAction(i));
		}

		return list;
	}

	public static ActionList createBlockElementActionList() {
		ActionList list = new ActionList("paragraph");
		//list.add(new HTMLBlockAction(HTMLBlockAction.DIV));
		list.add(new HTMLBlockAction(HTMLBlockAction.P));
		list.add(null);
		list.add(new HTMLBlockAction(HTMLBlockAction.BLOCKQUOTE));
		list.add(new HTMLBlockAction(HTMLBlockAction.PRE));
		list.add(null);
		list.add(new HTMLBlockAction(HTMLBlockAction.H1));
		list.add(new HTMLBlockAction(HTMLBlockAction.H2));
		list.add(new HTMLBlockAction(HTMLBlockAction.H3));
		list.add(new HTMLBlockAction(HTMLBlockAction.H4));
		list.add(new HTMLBlockAction(HTMLBlockAction.H5));
		list.add(new HTMLBlockAction(HTMLBlockAction.H6));
		return list;
	}

	public static ActionList createListElementActionList() {
		ActionList list = new ActionList("list");
		list.add(new HTMLBlockAction(HTMLBlockAction.UL));
		list.add(new HTMLBlockAction(HTMLBlockAction.OL));
		return list;
	}

	public static ActionList createInsertActionList() {
		ActionList list = new ActionList("insertActions");
		list.add(new HTMLLinkAction());
		list.add(new HTMLImageAction());
		list.add(new HTMLTableAction());
		list.add(null);
		list.add(new HTMLLineBreakAction());
		list.add(new HTMLHorizontalRuleAction());
		list.add(new HTMLUnicodeAction());
		return list;
	}

	public static ActionList createInsertTableElementActionList() {
		ActionList list = new ActionList("Insert into table");
		list.add(new HTMLTableEditAction(HTMLTableEditAction.INSERT_CELL));
		list.add(new HTMLTableEditAction(HTMLTableEditAction.INSERT_ROW));
		list.add(new HTMLTableEditAction(HTMLTableEditAction.INSERT_COL));
		return list;
	}

	public static ActionList createDeleteTableElementActionList() {
		ActionList list = new ActionList("Insert into table");
		list.add(new HTMLTableEditAction(HTMLTableEditAction.DELETE_CELL));
		list.add(new HTMLTableEditAction(HTMLTableEditAction.DELETE_ROW));
		list.add(new HTMLTableEditAction(HTMLTableEditAction.DELETE_COL));
		return list;
	}

}
