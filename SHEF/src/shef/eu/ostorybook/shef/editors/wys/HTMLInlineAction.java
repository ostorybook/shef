/*
 * Created on Feb 25, 2005
 *
 */
package eu.ostorybook.shef.editors.wys;

import eu.ostorybook.shef.actions.CompoundUndoManager;
import eu.ostorybook.shef.resources.images.Images;
import eu.ostorybook.shef.tools.HTMLUtils;
import java.awt.event.ActionEvent;
import java.awt.event.InputEvent;
import java.awt.event.KeyEvent;
import javax.swing.Icon;
import javax.swing.JEditorPane;
import javax.swing.KeyStroke;
import javax.swing.text.AttributeSet;
import javax.swing.text.MutableAttributeSet;
import javax.swing.text.SimpleAttributeSet;
import javax.swing.text.StyleConstants;
import javax.swing.text.html.CSS;
import javax.swing.text.html.HTML;

/**
 * Action which toggles inline HTML elements
 *
 * @author Bob Tantlinger
 *
 */
public class HTMLInlineAction extends HTMLTextEditAction {

	public static final int
			EM = 0,
			STRONG = 1,
			CODE = 2,
			CITE = 3,
			SUP = 4,
			SUB = 5,
			BOLD = 6,
			ITALIC = 7,
			UNDERLINE = 8,
			STRIKE = 9;

	public static final String[] TYPES = {
				"emphasis",
				"strong",
				"code",
				"cite",
				"superscript",
				"subscript",
				"bold",
				"italic",
				"underline",
				"strike"
			};

	private int type;

	/**
	 * Creates a new HTMLInlineAction
	 *
	 * @param itype an inline element type (BOLD, ITALIC, STRIKE, etc)
	 * @throws IllegalArgumentException
	 */
	public HTMLInlineAction(int itype) throws IllegalArgumentException {
		super("");
		type = itype;
		if (type < 0 || type >= TYPES.length) {
			throw new IllegalArgumentException("Illegal Argument");
		}
		putValue(NAME, TYPES[type]);
		setMnemonic(i18n.mnem(TYPES[type]));
		this.setShortDescription(i18n.str(TYPES[type]));

		Icon ico = null;
		KeyStroke ks = null;
		switch (type) {
			case BOLD:
				ico = Images.getIcon("text_bold");
				ks = KeyStroke.getKeyStroke(KeyEvent.VK_B, InputEvent.CTRL_DOWN_MASK);
				break;
			case ITALIC:
				ico = Images.getIcon("text_italic");
				ks = KeyStroke.getKeyStroke(KeyEvent.VK_I, InputEvent.CTRL_DOWN_MASK);
				break;
			case UNDERLINE:
				ico = Images.getIcon("text_underline");
				ks = KeyStroke.getKeyStroke(KeyEvent.VK_U, InputEvent.CTRL_DOWN_MASK);
				break;
			case STRIKE:
				ico = Images.getIcon("text_strike");
				ks = KeyStroke.getKeyStroke(KeyEvent.VK_S, InputEvent.CTRL_DOWN_MASK);
				break;
			case SUB:
				ico = Images.getIcon("text_subscript");
				break;
			case SUP:
				ico = Images.getIcon("text_superscript");
				break;
			default:
				break;
		}
		setSmallIcon(ico);
		setAccelerator(ks);
	}

	@Override
	protected void updateWysiwygContextState(JEditorPane ed) {
		setSelected(isDefined(HTMLUtils.getCharacterAttributes(ed)));
	}

	public HTML.Tag getTag() {
		return getTagForType(type);
	}

	private HTML.Tag getTagForType(int type) {
		HTML.Tag tag = null;
		switch (type) {
			case EM:
				tag = HTML.Tag.EM;
				break;
			case STRONG:
				tag = HTML.Tag.STRONG;
				break;
			case CODE:
				tag = HTML.Tag.CODE;
				break;
			case SUP:
				tag = HTML.Tag.SUP;
				break;
			case SUB:
				tag = HTML.Tag.SUB;
				break;
			case CITE:
				tag = HTML.Tag.CITE;
				break;
			case BOLD:
				tag = HTML.Tag.B;
				break;
			case ITALIC:
				tag = HTML.Tag.I;
				break;
			case UNDERLINE:
				tag = HTML.Tag.U;
				break;
			case STRIKE:
				tag = HTML.Tag.STRIKE;
				break;
		}
		return tag;
	}

	@Override
	protected void wysiwygEditPerformed(ActionEvent e, JEditorPane editor) {
		CompoundUndoManager.beginCompoundEdit(editor.getDocument());
		toggleStyle(editor);
		CompoundUndoManager.endCompoundEdit(editor.getDocument());
	}

	private boolean isDefined(AttributeSet attr) {
		boolean hasSC = false;
		switch (type) {
			case BOLD:
				hasSC = StyleConstants.isBold(attr);
				break;
			case ITALIC:
				hasSC = StyleConstants.isItalic(attr);
				break;
			case STRIKE:
				hasSC = StyleConstants.isStrikeThrough(attr);
				break;
			case SUB:
				hasSC = StyleConstants.isSubscript(attr);
				break;
			case SUP:
				hasSC = StyleConstants.isSuperscript(attr);
				break;
			case UNDERLINE:
				hasSC = StyleConstants.isUnderline(attr);
				break;
			default:
				break;
		}
		return hasSC || (attr.getAttribute(getTag()) != null);
	}

	private void toggleStyle(JEditorPane editor) {
		MutableAttributeSet attr = new SimpleAttributeSet();
		attr.addAttributes(HTMLUtils.getCharacterAttributes(editor));
		boolean enable = !isDefined(attr);
		HTML.Tag tag = getTag();
		if (enable) {
			attr = new SimpleAttributeSet();
			attr.addAttribute(tag, new SimpleAttributeSet());
			//doesn't replace any attribs, just adds the new one
			HTMLUtils.setCharacterAttributes(editor, attr);
		} else {
			//Kind of a ham-fisted way to do this, but sometimes there are
			//CSS attributes, someties there are HTML.Tag attributes, and sometimes
			//there are both. So, we have to remove 'em all to make sure this type
			//gets completely disabled
			//remove the CSS style
			//STRONG, EM, CITE, CODE have no CSS analogs
			switch (type) {
				case BOLD:
					HTMLUtils.removeCharacterAttribute(editor, CSS.Attribute.FONT_WEIGHT, "bold");
					break;
				case ITALIC:
					HTMLUtils.removeCharacterAttribute(editor, CSS.Attribute.FONT_STYLE, "italic");
					break;
				case UNDERLINE:
					HTMLUtils.removeCharacterAttribute(editor, CSS.Attribute.TEXT_DECORATION, "underline");
					break;
				case STRIKE:
					HTMLUtils.removeCharacterAttribute(editor, CSS.Attribute.TEXT_DECORATION, "line-through");
					break;
				case SUP:
					HTMLUtils.removeCharacterAttribute(editor, CSS.Attribute.VERTICAL_ALIGN, "sup");
					break;
				case SUB:
					HTMLUtils.removeCharacterAttribute(editor, CSS.Attribute.VERTICAL_ALIGN, "sub");
					break;
				default:
					break;
			}
			//make certain the tag is also removed
			HTMLUtils.removeCharacterAttribute(editor, tag);
		}
		setSelected(enable);
	}

}
