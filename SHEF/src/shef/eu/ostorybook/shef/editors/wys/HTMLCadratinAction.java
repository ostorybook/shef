/*
 * Created on Mar 3, 2005
 *
 */
package eu.ostorybook.shef.editors.wys;

import eu.ostorybook.shef.resources.images.Images;
import java.awt.event.ActionEvent;
import javax.swing.JEditorPane;
/**
 * Action which inserts special characters like cadratin
 *
 * @author Bob Tantlinger
 *
 */
public class HTMLCadratinAction extends HTMLTextEditAction {

	public static final int
			CADRATIN = 0,
			DIALOG_OPEN_EN = 1,
			DIALOG_CLOSE_EN = 2,
			DIALOG_OPEN_FR = 3,
			DIALOG_CLOSE_FR = 4;
	public static final String[] TYPES = {
				"cadratin",
				"dialog_open_en",
				"dialog_close_en",
				"dialog_open_fr",
				"dialog_close_fr"
			};
	private int type;

	public HTMLCadratinAction(int c) {
		super(TYPES[c]);
		setMnemonic(i18n.mnem(TYPES[c]));
		setShortDescription(i18n.str(TYPES[c]));
		setSmallIcon(Images.getIcon(TYPES[c]));
		type = c;
	}

	@Override
	protected void wysiwygEditPerformed(ActionEvent e, JEditorPane editor) {
		int caret = editor.getCaretPosition();
		String c;
		switch(type) {
			case 0: c="— "; break;
			case 1: c="“ "; break;
			case 2: c="” ";break;
			case 3: c="« "; break;
			case 4: c=" »";break;
			default: return;
		}
		if (caret < 1) {
			return;
		}
		String x = editor.getSelectedText();
		if (x == null || x.isEmpty()) {
			editor.setSelectionStart(caret);
			editor.setSelectionEnd(caret);
		}
		editor.replaceSelection(c);
	}
}
