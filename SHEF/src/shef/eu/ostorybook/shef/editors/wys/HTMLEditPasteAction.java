/*
 * Created on Jun 19, 2005
 *
 */
package eu.ostorybook.shef.editors.wys;

import eu.ostorybook.shef.actions.CompoundUndoManager;
import java.awt.Toolkit;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.Transferable;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.awt.event.ActionEvent;
import java.awt.event.InputEvent;
import java.awt.event.KeyEvent;
import java.io.IOException;
import javax.swing.Action;
import javax.swing.JEditorPane;
import javax.swing.KeyStroke;
import javax.swing.text.BadLocationException;
import javax.swing.text.html.HTMLDocument;
import javax.swing.text.html.HTMLEditorKit;

public class HTMLEditPasteAction extends HTMLTextEditAction {

	public HTMLEditPasteAction() {
		super(i18n.str("paste"));
		putValue(MNEMONIC_KEY, i18n.mnem("paste"));
		setSmallIcon("edit_paste");
		setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_V, InputEvent.CTRL_DOWN_MASK));
		addShouldBeEnabledDelegate((Action a) -> true);
		setShortDescription(i18n.str("paste"));
	}

	@Override
	protected void updateWysiwygContextState(JEditorPane wysEditor) {
		this.updateEnabledState();
	}

	@Override
	protected void wysiwygEditPerformed(ActionEvent e, JEditorPane editor) {
		HTMLEditorKit ekit = (HTMLEditorKit) editor.getEditorKit();
		HTMLDocument document = (HTMLDocument) editor.getDocument();
		Clipboard clip = Toolkit.getDefaultToolkit().getSystemClipboard();
		try {
			CompoundUndoManager.beginCompoundEdit(document);
			Transferable content = clip.getContents(this);
			String txt = content.getTransferData(
					new DataFlavor(String.class, "String")).toString();

			document.replace(editor.getSelectionStart(),
					editor.getSelectionEnd() - editor.getSelectionStart(),
					txt, ekit.getInputAttributes());
		} catch (UnsupportedFlavorException | IOException | BadLocationException ex) {
		} finally {
			CompoundUndoManager.endCompoundEdit(document);
		}
	}

}
