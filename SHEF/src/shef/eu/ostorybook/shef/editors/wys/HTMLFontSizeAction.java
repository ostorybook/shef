/*
 * Created on Feb 27, 2005
 *
 */
package eu.ostorybook.shef.editors.wys;

import eu.ostorybook.shef.tools.HTMLUtils;
import java.awt.event.ActionEvent;
import javax.swing.Action;
import javax.swing.JEditorPane;
import javax.swing.text.AttributeSet;
import javax.swing.text.StyleConstants;
import javax.swing.text.StyledEditorKit;

/**
 * Action which edits HTML font size
 *
 * @author Bob Tantlinger
 *
 */
public class HTMLFontSizeAction extends HTMLTextEditAction {

	public static final int XXSMALL = 0;
	public static final int XSMALL = 1;
	public static final int SMALL = 2;
	public static final int MEDIUM = 3;
	public static final int LARGE = 4;
	public static final int XLARGE = 5;
	public static final int XXLARGE = 6;
	private static final String SML = i18n.str("small");
	private static final String MED = i18n.str("medium");
	private static final String LRG = i18n.str("large");
	public static final int FONT_SIZES[] = {8, 10, 12, 14, 18, 24, 36/*28*/};
	public static final String SIZES[] = {
				"xx-" + SML, "x-" + SML, SML, MED,
				LRG, "x-" + LRG, "xx-" + LRG
			};
	private int size;

	/**
	 * Creates a new HTMLFontSizeAction
	 *
	 * @param size one of the FONT_SIZES (XXSMALL, xSMALL, SMALL, MEDIUM, LARGE, XLARGE, XXLARGE)
	 *
	 * @throws IllegalArgumentException
	 */
	public HTMLFontSizeAction(int size) throws IllegalArgumentException {
		super("");
		if (size < 0 || size > 6) {
			throw new IllegalArgumentException("Invalid size");
		}
		this.size = size;
		putValue(NAME, SIZES[size]);
		putValue(Action.SHORT_DESCRIPTION, getValue(Action.NAME));
	}

	@Override
	protected void updateWysiwygContextState(JEditorPane ed) {
		AttributeSet at = HTMLUtils.getCharacterAttributes(ed);
		if (at.isDefined(StyleConstants.FontSize)) {
			setSelected(at.containsAttribute(StyleConstants.FontSize, FONT_SIZES[size]));
		} else {
			setSelected(size == MEDIUM);
		}
	}

	@Override
	protected void wysiwygEditPerformed(ActionEvent e, JEditorPane editor) {
		Action a = new StyledEditorKit.FontSizeAction(SIZES[size], FONT_SIZES[size]);
		a.actionPerformed(e);
	}
}
