/*
 * Created on Jan 13, 2006
 *
 */
package eu.ostorybook.shef.editors.wys;

import eu.ostorybook.shef.dialogs.ImageDialog;
import eu.ostorybook.shef.resources.images.Images;
import eu.ostorybook.shef.tools.HTMLUtils;
import java.awt.Dialog;
import java.awt.Frame;
import java.awt.Window;
import java.awt.event.ActionEvent;
import javax.swing.JEditorPane;
import javax.swing.SwingUtilities;
import javax.swing.text.JTextComponent;
import javax.swing.text.html.HTML;

/**
 * Action which desplays a dialog to insert an image
 *
 * @author Bob Tantlinger
 *
 */
public class HTMLImageAction extends HTMLTextEditAction {

	public HTMLImageAction() {
		super(i18n.str("image_"));
		setSmallIcon(Images.getIcon("image"));
		setShortDescription(i18n.str("image_desc"));
	}

	@Override
	protected void wysiwygEditPerformed(ActionEvent e, JEditorPane editor) {
		ImageDialog d = createDialog(editor);
		d.setLocationRelativeTo(d.getParent());
		d.setVisible(true);
		if (d.hasUserCancelled()) {
			return;
		}
		String tagText = d.getHTML();
		if (editor.getCaretPosition() == editor.getDocument().getLength()) {
			tagText += "&nbsp;";
		}
		editor.replaceSelection("");
		HTML.Tag tag = HTML.Tag.IMG;
		if (tagText.startsWith("<a")) {
			tag = HTML.Tag.A;
		}
		HTMLUtils.insertHTML(tagText, tag, editor);
	}

	protected ImageDialog createDialog(JTextComponent ed) {
		Window w = SwingUtilities.getWindowAncestor(ed);
		ImageDialog d = null;
		if (w != null && w instanceof Frame) {
			d = new ImageDialog((Frame) w);
		} else if (w != null && w instanceof Dialog) {
			d = new ImageDialog((Dialog) w);
		}
		return d;
	}

}
