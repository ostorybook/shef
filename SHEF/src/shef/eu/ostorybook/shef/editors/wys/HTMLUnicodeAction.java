/*
 * Created on Jan 24, 2006
 *
 */
package eu.ostorybook.shef.editors.wys;

import eu.ostorybook.shef.dialogs.SpecialCharDialog;
import eu.ostorybook.shef.resources.images.Images;
import java.awt.Component;
import java.awt.Dialog;
import java.awt.Frame;
import java.awt.event.ActionEvent;
import javax.swing.JEditorPane;
import javax.swing.SwingUtilities;

public class HTMLUnicodeAction extends HTMLTextEditAction {

	SpecialCharDialog dialog;

	public HTMLUnicodeAction() {
		super(i18n.str("unicode"));
		setSmallIcon(Images.getIcon("char_unicode"));
		setShortDescription(i18n.str("unicode_desc"));
	}

	protected void doEdit(ActionEvent e, JEditorPane ed) {
		Component c = SwingUtilities.getWindowAncestor(ed);
		if (dialog == null) {
			if (c instanceof Frame) {
				dialog = new SpecialCharDialog((Frame) c, ed);
			} else if (c instanceof Dialog) {
				dialog = new SpecialCharDialog((Dialog) c, ed);
			} else {
				return;
			}
		}
		dialog.setInsertEntity(getEditMode() == SOURCE);
		if (!dialog.isVisible()) {
			dialog.setLocationRelativeTo(c);
			dialog.setVisible(true);
		}
	}

	protected void updateContextState(JEditorPane editor) {
		if (dialog != null) {
			dialog.setInsertEntity(getEditMode() == SOURCE);
			dialog.setJTextComponent(editor);
		}
	}

	@Override
	protected void wysiwygEditPerformed(ActionEvent e, JEditorPane editor) {
		doEdit(e, editor);
	}

	@Override
	protected final void updateWysiwygContextState(JEditorPane wysEditor) {
		updateContextState(wysEditor);
	}

}
