/*
 * Created on december, 2020
 */
package eu.ostorybook.shef.editors.src;

import java.awt.event.ActionEvent;
import javax.swing.JEditorPane;

/**
 * @author favdb
 *
 */
public class SRCIndentAction extends SRCAbstractAction {

	public static final int INDENT = 0;
	public static final int OUTDENT = 1;
	protected int direction;

	/**
	 * @param name
	 */
	public SRCIndentAction(int direction) throws IllegalArgumentException {
		super("");
		switch (direction) {
			case INDENT:
				putValue(NAME, "Indent");
				break;
			case OUTDENT:
				putValue(NAME, "Outdent");
				break;
			default:
				throw new IllegalArgumentException("Invalid indentation direction");
		}
		this.direction = direction;
	}

	@Override
	protected void sourceEditPerformed(ActionEvent e, JEditorPane editor) {
	}

}
