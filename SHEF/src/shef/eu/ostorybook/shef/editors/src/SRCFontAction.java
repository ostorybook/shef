/*
 * Created on Jan 18, 2006
 *
 */
package eu.ostorybook.shef.editors.src;

import eu.ostorybook.shef.dialogs.HTMLFontDialog;
import java.awt.Dialog;
import java.awt.Frame;
import java.awt.Window;
import java.awt.event.ActionEvent;
import javax.swing.JEditorPane;
import javax.swing.SwingUtilities;
import javax.swing.text.JTextComponent;

/**
 * Action which edits an HTML font
 *
 * @author Bob Tantlinger
 *
 */
public class SRCFontAction extends SRCAbstractAction {

	public SRCFontAction() {
		super(i18n.str("font_"));
	}

	@Override
	protected void sourceEditPerformed(ActionEvent e, JEditorPane editor) {
		HTMLFontDialog d = createDialog(editor);
		d.setLocationRelativeTo(d.getParent());
		d.setVisible(true);
		if (!d.hasUserCancelled()) {
			editor.requestFocusInWindow();
			editor.replaceSelection(d.getHTML());
		}
	}

	private HTMLFontDialog createDialog(JTextComponent ed) {
		Window w = SwingUtilities.getWindowAncestor(ed);
		String t = "";
		if (ed.getSelectedText() != null) {
			t = ed.getSelectedText();
		}
		HTMLFontDialog d = null;
		if (w != null && w instanceof Frame) {
			d = new HTMLFontDialog((Frame) w, t);
		} else if (w != null && w instanceof Dialog) {
			d = new HTMLFontDialog((Dialog) w, t);
		}
		return d;
	}

}
