/*
 * Created on Feb 26, 2005
 *
 */
package eu.ostorybook.shef.editors.src;

import eu.ostorybook.shef.dialogs.NewTableDialog;
import eu.ostorybook.shef.resources.images.Images;
import java.awt.Dialog;
import java.awt.Frame;
import java.awt.Window;
import java.awt.event.ActionEvent;
import javax.swing.JEditorPane;
import javax.swing.SwingUtilities;
import javax.swing.text.JTextComponent;

/**
 * Action which shows a dialog to insert an HTML table
 *
 * @author Bob Tantlinger
 *
 */
public class SRCTableAction extends SRCAbstractAction {

	public SRCTableAction() {
		super(i18n.str("table_"));
		setMnemonic(i18n.mnem("table_"));
		setSmallIcon(Images.getIcon("table"));
		setShortDescription(i18n.str("table_desc"));
	}

	@Override
	protected void sourceEditPerformed(ActionEvent e, JEditorPane editor) {
		NewTableDialog dlg = createDialog(editor);
		if (dlg == null) {
			return;
		}
		dlg.setLocationRelativeTo(dlg.getParent());
		dlg.setVisible(true);
		if (dlg.hasUserCancelled()) {
			return;
		}
		editor.replaceSelection(dlg.getHTML());
	}

	/**
	 * Creates the dialog
	 *
	 * @param ed
	 * @return the dialog
	 */
	private NewTableDialog createDialog(JTextComponent ed) {
		Window w = SwingUtilities.getWindowAncestor(ed);
		NewTableDialog d = null;
		if (w != null && w instanceof Frame) {
			d = new NewTableDialog((Frame) w);
		} else if (w != null && w instanceof Dialog) {
			d = new NewTableDialog((Dialog) w);
		}
		return d;
	}

}
