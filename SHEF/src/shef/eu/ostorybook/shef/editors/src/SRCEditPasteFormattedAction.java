/*
 * Created on Nov 25, 2007
 */
package eu.ostorybook.shef.editors.src;

import eu.ostorybook.shef.actions.*;
import eu.ostorybook.shef.resources.images.Images;
import java.awt.Toolkit;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.Transferable;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.awt.event.ActionEvent;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.Reader;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.swing.Action;
import javax.swing.JEditorPane;
import javax.swing.KeyStroke;

/**
 * @author Bob Tantlinger
 *
 */
public class SRCEditPasteFormattedAction extends SRCAbstractAction {

	/**
	 * @param name
	 */
	public SRCEditPasteFormattedAction() {

		super(i18n.str("paste_formatted"));
		putValue(MNEMONIC_KEY, i18n.mnem("paste_formatted"));
		putValue(SMALL_ICON, Images.getIcon("edit_paste"));
		putValue(ACCELERATOR_KEY, KeyStroke.getKeyStroke("shift ctrl V"));
		addShouldBeEnabledDelegate((Action a) -> {
			if (getCurrentEditor() == null) {
				return false;
			}
			Transferable content
					= Toolkit.getDefaultToolkit().getSystemClipboard().getContents(SRCEditPasteFormattedAction.this);

			if (content == null) {
				return false;
			}
			DataFlavor flv = DataFlavor.selectBestTextFlavor(content.getTransferDataFlavors());
			return flv != null && flv.getMimeType().startsWith("text/html");
		});
		putValue(Action.SHORT_DESCRIPTION, getValue(Action.NAME));

	}

	@Override
	protected void updateSourceContextState(JEditorPane srcEditor) {
		this.updateEnabledState();
	}

	@Override
	protected void sourceEditPerformed(ActionEvent e, JEditorPane editor) {
		String htmlFragment = null;
		try {
			htmlFragment = getHTMLFragment();
		} catch (UnsupportedFlavorException | IOException ex) {
			ex.printStackTrace(System.err);
		}
		if (htmlFragment != null) {
			CompoundUndoManager.beginCompoundEdit(editor.getDocument());
			editor.replaceSelection(htmlFragment);
			CompoundUndoManager.endCompoundEdit(editor.getDocument());
		}
	}

	/**
	 * Get the HTML text from the content if any
	 *
	 * @return returns the html fragment, or null if this content isn't HTML
	 * @throws UnsupportedFlavorException
	 * @throws IOException
	 */
	private String getHTMLFragment() throws IOException, UnsupportedFlavorException {
		Clipboard clip = Toolkit.getDefaultToolkit().getSystemClipboard();
		Transferable c = clip.getContents(this);
		if (c == null) {
			return null;
		}
		DataFlavor flv = DataFlavor.selectBestTextFlavor(c.getTransferDataFlavors());
		if (!flv.getMimeType().startsWith("text/html")) {
			return null;
		}
		String text = read((flv.getReaderForText(c)));
		//when html content is retrieved from the transferable, the copied part
		//is enclosed in a <body> tag, so only get the contents we want...
		int flags = Pattern.CASE_INSENSITIVE | Pattern.UNICODE_CASE;
		Pattern p = Pattern.compile(("<\\s*body\\b([^<>]*)>"), flags);
		Matcher m = p.matcher(text);
		if (m.find()) {
			text = text.substring(m.end(), text.length());
		}
		p = Pattern.compile("<\\s*/\\s*body\\s*>", flags);
		m = p.matcher(text);
		if (m.find()) {
			text = text.substring(0, m.start());
		}
		//when html content is retrieved from the transferable, the copied part
		//is surrounded with the comments <!--StartFragment--> and <!--EndFragment--> on windows        
		text = text.replaceAll("<\\!\\-\\-StartFragment\\-\\->", "");
		text = text.replaceAll("<\\!\\-\\-EndFragment\\-\\->", "");
		//gets rid of 'class' and 'id' attributes in the tags.
		//It really doesn't make much sense to include these attribs in HTML
		//pasted in from the wild.
		String r = "<([^>]*)(?:class|id)\\s*=\\s*(?:'[^']*'|\"\"[^\"\"]*\"\"|[^\\s>]+)([^>]*)>";
		p = Pattern.compile(r, flags);
		//run it twice for each attrib
		m = p.matcher(text);
		text = m.replaceAll("<$1$2>");
		m = p.matcher(text);
		text = m.replaceAll("<$1$2>");
		return text;
	}

	public String read(Reader input) throws IOException {
		BufferedReader reader = new BufferedReader(input);
		StringBuilder b = new StringBuilder();
		int ch;
		try {
			while ((ch = reader.read()) != -1) {
				b.append((char) ch);
			}
		} catch (IOException ex) {
			throw ex;
		} finally {
			try {
				reader.close();
			} catch (IOException ex) {
			}
		}
		return b.toString();
	}

}
