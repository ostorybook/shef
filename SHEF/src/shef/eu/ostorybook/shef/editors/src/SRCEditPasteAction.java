/*
 * Created on Jun 19, 2005
 *
 */
package eu.ostorybook.shef.editors.src;

import eu.ostorybook.shef.actions.*;
import java.awt.Toolkit;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.Transferable;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.awt.event.ActionEvent;
import java.awt.event.InputEvent;
import java.awt.event.KeyEvent;
import java.io.IOException;
import javax.swing.Action;
import javax.swing.JEditorPane;
import javax.swing.KeyStroke;
import javax.swing.text.BadLocationException;
import javax.swing.text.html.HTMLDocument;
import javax.swing.text.html.HTMLEditorKit;

public class SRCEditPasteAction extends SRCAbstractAction {

	public SRCEditPasteAction() {
		super(i18n.str("paste"));
		putValue(MNEMONIC_KEY, i18n.mnem("paste"));
		setSmallIcon("edit_paste");
		setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_V, InputEvent.CTRL_DOWN_MASK));
		addShouldBeEnabledDelegate((Action a) -> true);
		setShortDescription(i18n.str("paste"));
	}

	@Override
	protected void updateSourceContextState(JEditorPane srcEditor) {
		updateEnabledState();
	}

	@Override
	protected void sourceEditPerformed(ActionEvent e, JEditorPane editor) {
		editor.paste();
	}

}
