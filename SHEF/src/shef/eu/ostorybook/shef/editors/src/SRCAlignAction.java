/*
 * Created on Feb 25, 2005
 *
 */
package eu.ostorybook.shef.editors.src;

import eu.ostorybook.shef.actions.*;
import eu.ostorybook.shef.tools.HTMLUtils;
import java.awt.event.ActionEvent;
import java.awt.event.InputEvent;
import java.io.IOException;
import javax.swing.JEditorPane;
import javax.swing.KeyStroke;
import javax.swing.text.AttributeSet;
import javax.swing.text.BadLocationException;
import javax.swing.text.Element;
import javax.swing.text.MutableAttributeSet;
import javax.swing.text.SimpleAttributeSet;
import javax.swing.text.html.HTML;
import javax.swing.text.html.HTMLDocument;

/**
 * Action which aligns HTML elements
 *
 * @author Bob Tantlinger
 *
 */
public class SRCAlignAction extends SRCAbstractAction {

	public static final int LEFT = 0;
	public static final int CENTER = 1;
	public static final int RIGHT = 2;
	public static final int JUSTIFY = 3;
	public static final String ALIGNMENT_NAMES[] = {
		i18n.str("left"),
		i18n.str("center"),
		i18n.str("right"),
		i18n.str("justify")
	};
	private static final int[] MNEMS = {
		i18n.mnem("left"),
		i18n.mnem("center"),
		i18n.mnem("right"),
		i18n.mnem("justify")
	};
	public static final String ALIGNMENTS[] = {"left", "center", "right", "justify"};
	private int align;

	/**
	 * Creates a new HTMLAlignAction
	 *
	 * @param al LEFT, RIGHT, CENTER, or JUSTIFY
	 * @throws IllegalArgumentException
	 */
	public SRCAlignAction(int al) throws IllegalArgumentException {
		super(ALIGNMENT_NAMES[al]);
		if (al < 0 || al >= ALIGNMENTS.length) {
			throw new IllegalArgumentException("Illegal Argument");
		}
		putValue(NAME, ALIGNMENT_NAMES[al]);
		setMnemonic(MNEMS[al]);
		setShortDescription(ALIGNMENT_NAMES[al]);
		setAccelerator(KeyStroke.getKeyStroke(ALIGNMENTS[al].charAt(0), InputEvent.ALT_DOWN_MASK));
		setSmallIcon("al_"+ALIGNMENTS[al]);
		align = al;
	}

	@Override
	protected void updateSourceContextState(JEditorPane ed) {
		setSelected(false);
	}

	private void alignElement(Element elem) {
		HTMLDocument doc = (HTMLDocument) elem.getDocument();
		if (HTMLUtils.isImplied(elem)) {
			HTML.Tag tag = HTML.getTag(elem.getParentElement().getName());
			//pre tag doesn't support an align attribute
			//http://www.w3.org/TR/REC-html32#pre
			if (tag != null && (!tag.equals(HTML.Tag.BODY))
					&& (!tag.isPreformatted() && !tag.equals(HTML.Tag.DD))) {
				SimpleAttributeSet as = new SimpleAttributeSet(elem.getAttributes());
				as.removeAttribute("align");
				as.addAttribute("align", ALIGNMENTS[align]);
				Element parent = elem.getParentElement();
				String html = HTMLUtils.getElementHTML(elem, false);
				html = HTMLUtils.createTag(tag, as, html);
				String snipet = "";
				for (int i = 0; i < parent.getElementCount(); i++) {
					Element el = parent.getElement(i);
					if (el == elem) {
						snipet += html;
					} else {
						snipet += HTMLUtils.getElementHTML(el, true);
					}
				}
				try {
					doc.setOuterHTML(parent, snipet);
				} catch (IOException | BadLocationException ex) {
					ex.printStackTrace(System.err);
				}
			}
		} else {
			//Set the HTML attribute on the paragraph...
			MutableAttributeSet set = new SimpleAttributeSet(elem.getAttributes());
			set.removeAttribute(HTML.Attribute.ALIGN);
			set.addAttribute(HTML.Attribute.ALIGN, ALIGNMENTS[align]);
			//Set the paragraph attributes...
			int start = elem.getStartOffset();
			int length = elem.getEndOffset() - elem.getStartOffset();
			doc.setParagraphAttributes(start, length - 1, set, true);
		}
	}

	@Override
	protected void sourceEditPerformed(ActionEvent e, JEditorPane editor) {
		String prefix = "<p align=\"" + ALIGNMENTS[align] + "\">";
		String postfix = "</p>";
		String sel = editor.getSelectedText();
		if (sel == null) {
			editor.replaceSelection(prefix + postfix);
			int pos = editor.getCaretPosition() - postfix.length();
			if (pos >= 0) {
				editor.setCaretPosition(pos);
			}
		} else {
			sel = prefix + sel + postfix;
			editor.replaceSelection(sel);
		}
	}

}
