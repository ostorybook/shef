/*
 * Created on Feb 27, 2005
 *
 */
package eu.ostorybook.shef.editors.src;

import java.awt.event.ActionEvent;
import javax.swing.Action;
import javax.swing.JEditorPane;

/**
 * Action which edits HTML font size
 *
 * @author favdb
 *
 */
public class SRCFontSizeAction extends SRCAbstractAction {

	public static final int XXSMALL = 0;
	public static final int XSMALL = 1;
	public static final int SMALL = 2;
	public static final int MEDIUM = 3;
	public static final int LARGE = 4;
	public static final int XLARGE = 5;
	public static final int XXLARGE = 6;
	private static final String SML = i18n.str("small");
	private static final String MED = i18n.str("medium");
	private static final String LRG = i18n.str("large");
	public static final int FONT_SIZES[] = {8, 10, 12, 14, 18, 24, 36/*28*/};
	public static final String SIZES[] = {
				"xx-" + SML, "x-" + SML, SML, MED,
				LRG, "x-" + LRG, "xx-" + LRG
			};
	private int size;

	/**
	 * Creates a new HTMLFontSizeAction
	 *
	 * @param size one of the FONT_SIZES (XXSMALL, xSMALL, SMALL, MEDIUM, LARGE, XLARGE, XXLARGE)
	 *
	 * @throws IllegalArgumentException
	 */
	public SRCFontSizeAction(int size) throws IllegalArgumentException {
		super("");
		if (size < 0 || size > 6) {
			throw new IllegalArgumentException("Invalid size");
		}
		this.size = size;
		putValue(NAME, SIZES[size]);
		putValue(Action.SHORT_DESCRIPTION, getValue(Action.NAME));
	}

	@Override
	protected void updateSourceContextState(JEditorPane ed) {
		setSelected(false);
	}

	@Override
	protected void sourceEditPerformed(ActionEvent e, JEditorPane editor) {
		String prefix = "<font size=" + (size + 1) + ">";
		String postfix = "</font>";
		String sel = editor.getSelectedText();
		if (sel == null) {
			editor.replaceSelection(prefix + postfix);

			int pos = editor.getCaretPosition() - postfix.length();
			if (pos >= 0) {
				editor.setCaretPosition(pos);
			}
		} else {
			sel = prefix + sel + postfix;
			editor.replaceSelection(sel);
		}
	}

}
